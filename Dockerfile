#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV FLUENTD_VERSION="1.16.6"
ENV GEM_HOME=/usr/local/ruby/gems
ENV GEM_PATH=$GEM_HOME
ENV GEM_CACHE=$GEM_HOME/cache
ENV PATH=$PATH:$GEM_HOME/bin

ENV BASE_PKGS libjemalloc2 ruby

# Update users and groups
RUN userdel ubuntu

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*

##########################
# Create build container #
##########################
FROM base AS builder

# Set build dependencies
ENV BUILD_DEPS autoconf build-essential libjemalloc-dev ruby-dev wget  

# Install build dependencies
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BUILD_DEPS} \
 && rm -rf /var/lib/apt/lists/*

# Run build
WORKDIR /usr/src
RUN echo 'gem: --no-document' >> /etc/gemrc
RUN gem install fluentd -v ${FLUENTD_VERSION}
RUN gem install json webrick etc oj nokogiri
RUN fluent-gem install fluent-plugin-concat -v 2.5.0
RUN fluent-gem install fluent-plugin-opensearch -v 1.1.4
RUN fluent-gem install fluent-plugin-kubernetes_metadata_filter -v 3.5.0
RUN fluent-gem install fluent-plugin-prometheus -v 2.2.0
RUN fluent-gem install fluent-plugin-rewrite-tag-filter -v 2.4.0
RUN fluent-gem install fluent-plugin-stats -v 0.4.0
RUN fluent-gem install fluent-plugin-systemd -v 1.1.0
RUN gem sources --clear-all
RUN rm -rf $GEM_HOME/*/cache/*.gem


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY --from=builder /usr/local /usr/local/
COPY usr/ /usr/

ENV LD_PRELOAD="/usr/lib//x86_64-linux-gnu/libjemalloc.so.2"
VOLUME ["/etc/fluent"]

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["fluentd"]
