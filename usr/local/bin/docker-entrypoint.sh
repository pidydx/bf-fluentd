#!/bin/bash

set -e

if [ "$1" = 'fluent' ]; then
    exec fluentd
fi

if [ "$1" = 'init' ]; then
    exec chmod o-w /tmp
fi

exec "$@"
